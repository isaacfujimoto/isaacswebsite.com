var bird;


function setup(){
	createCanvas(windowWidth, windowHeight);
	bird = new Bird();
}

function draw(){
	background(138, 43, 226);
	bird.update();
	bird.show();

	show_height();
	fill(0);
	textSize(15);
	text("Hi!!! Press space so fast to make the Boiled egg fly. You can also make it move left and right by a and d keys. \nHAVE FUN!!!!!" , 20, 50);
}


function keyPressed(){
	if (key == " "){
		bird.up()
	}
	if (key == "a"){
		bird.left();
	}
	if (key == "d"){
		bird.right();
	}
}



function show_height(){
	fill(0);
	textSize(15);
	text("height: " + ((bird.y-windowHeight)*-1).toString(), 0, height-12);
}
