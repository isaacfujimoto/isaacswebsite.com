function Bird(){
	this.y = height/2;
	this.x = width/2;
        this.image = '';	

	this.gravity=1;
	this.velocity=0;

	this.show=function(){
		fill(255);
		//ellipse(this.x, this.y, 15, 15);
		image(this.image, this.x, this.y);

	}

	this.up = function(){
		this.velocity += -this.gravity*15;
		this.y += this.velocity;
	}

	this.left = function(){
		this.x -= 5;
	}

	this.right = function(){
		this.x += 5;
	}
	this.update = function(){
		this.velocity += this.gravity;
		this.y += this.velocity;


		if(this.y > height){
			this.y = height;
			this.velocity = 0;
		}

		if(this.y < 0){
		//	this.y = 0;
	//		this.velocity = 0;
		}
	}
}
