let angle=0;

function setup() {
	createCanvas(windowWidth, windowHeight, WEBGL);

	background(21);
}

function draw() {

	noStroke();
	rectMode(CENTER);
	fill(random(0, 255), random(0, 255), random(0, 255));

	rotateX(angle);
	rotateY(angle*0.3);
	rotateZ(angle*0.6);

	box(150);

	angle+=0.07;

}
