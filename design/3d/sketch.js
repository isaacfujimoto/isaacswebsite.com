let angle=0;
let fo;


function preload(){
}

function setup() {
	createCanvas(windowWidth, windowHeight, WEBGL);
	fill('#ED225D');

}

function draw() {

	background(21, 21, 50);
	noStroke();
	rectMode(CENTER);
	fill(random(0, 255), random(0, 255), random(0, 255));

	rotateX(angle);
	rotateY(angle * 0.3);
	rotateZ(angle * 1.2);


	push();
	translate(0, 0, random(0, 500));
	rotateX(frameCount*5.67);
	rotateY(frameCount*6.78);
	rotateZ(frameCount*7.89);
	cone(50, 50)
	pop();

	push();
	rotateX(frameCount*0.05);
	rotateY(frameCount*0.05);
	rotateY(frameCount*0.05);
	translate(0, 0, random(0, 300));
	plane(80, 60);
	pop();

	push();
	translate(-240*2, 200, 0);
	cylinder(10, 50);
	pop();

	push();
	translate(0, 0, random(0, 720));
	rotateZ(frameCount * 0.01);
	box(60, 50);
	pop();

	angle+=0.07;
}
