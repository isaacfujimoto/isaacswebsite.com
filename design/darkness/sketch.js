var song;
var img;

function preload(){
    song = loadSound('hellodarkness.mp3');
    img =  loadImage("darkness.jpg");
    image(img, 0, 0);
    // photo credit: https://www.pexels.com/photo/starry-sky-998641/
}

function setup() {
    // put setup code here
    createCanvas(1800, 800);
}

function mousePressed(){
    if(song.isPlaying()){
        song.stop();
    }else{
        song.play();
    }
}

function draw() {
  // put drawing code here

    image(img, 0, 0);
    if(mouseIsPressed){
        fill(0);
    }else{
        fill(255);
    }
    ellipse(mouseX, mouseY, 180, 180);
}

