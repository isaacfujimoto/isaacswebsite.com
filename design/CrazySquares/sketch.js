let messages; 

function setup(){
    createCanvas(windowWidth, windowWidth);
    messages = ["Random is ..." , " everywhere.", "Random appears", "in nature."] ;

}

let  mssg;
let i;
function draw(){
    background(0);
    //rect(random(1, 600), random(1, 600), random(1, 600), random(1, 600));
   rect(randomGaussian(mouseX, 50), randomGaussian(mouseY, 50), random(1, width/4), random(1, height/4))  ;
    mssg = random( messages);

    textSize(random(13, 29));
    text(mssg, random(0, width - 200), random(0, height));
    fill(0, 102, 153);

    
}
