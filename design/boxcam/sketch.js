let angle=0;
let cam;


let principal;
function preload(){
	principal = loadImage("P.png")
}

function setup() {
	createCanvas(600, 400, WEBGL);
	cam = createCapture(VIDEO);
}



function draw() {

	//pointLight(0, 0, 255, 0, -200, 0);
	//pointLight(0, 0, 255, -200,0, 0);

//	let dx = mouseX - width/2;

//	let dy = mouseX - height/2;
	
//	let v = createVector(dx, dy, 0);

//	v.div(100);

	ambientLight(255);
	//ambientLight(255, 200, 150);
	texture(cam);
	background(175);

	
	rotateX(angle);
	rotateY(angle*0.3);
	rotateZ(angle*1.2);

	noStroke();
	box(200, 200);

	angle+=0.03;
}
