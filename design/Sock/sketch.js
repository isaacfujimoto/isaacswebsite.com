let Fx = 100;
let Fy = 50;

let Cx = 100;
let Cy = 50;

let angryx = 150;
let angryy = 3;

let ax = 150;
let ay = 3;

let bx = 400;
let by =6;

let px = 100;
let py = 50;
 
let firstx = 8;
let firsty = 9;

let baldix = 555;
let baldiy = 10;

let gx = 2;
let gy = 15

let Px = 150;
let Py = 3;

let e;
let i;

let isPlayed;

//images
let Filenametwo;
let cloud;
let art;
let art2;
let Bully;
let prize;
let img;
let play;
let Baldi;
let Gottasweep;

//sounds
let greatjob;
let sndBaldi;
let playtime;
let see;
let artSound;
let slap;


function preload(){
    Filenametwo = loadImage("Filename2.png");
    cloud = loadImage("cloud.png");
    art = loadImage("art.png");
    art2 = loadImage("Angry.png");
    Bully = loadImage("Bully.png");
    prize = loadImage("1stprize.png");
    img = loadImage("principal.png");
    play = loadImage("Playtime.png");
    Baldi = loadImage("Baldi.png");
    Gottasweep = loadImage("gotta_sweep.png");

    greatjob = loadSound("WOW.mp3");
    sndBaldi = loadSound("while.mp3");
    sndBaldi = loadSound("Slap.mp3");
    playtime = loadSound("LETS PLAY.mp3");
    see = loadSound("first sound.mp3");
    artSound = loadSound("ArtsSound2.mp3");
    slap = loadSound("Slap.mp3");
    Great = loadSound("Give me something GREAT.mp3");
}


function setup(){
    createCanvas(windowWidth, windowHeight);

    sndBaldi.play();
    playtime.play();
    see.play();
    isPlayed = false;

    
}

function loaded(){
}

function changebackground(){
    background(250, i, e)
    i = 0;
    e = 10;
    if (i < 255) {
        i++;
    } 
    if (e < 255){
        e--;
    }
}


function draw(){
    changebackground();

    if (Fx < width &&Fy < height){
        console.log(Fx);
        Filenametwo_move();
    }

    if (Cx < width &&Cy < height){
        console.log(Cx);
        cloud_move();
    }

    if (firstx < width &&firsty < height){
        First_Prize();
    }

    if (Px < width && Py < height){
        Playtime_move();     
    }

    if (px < width && py < height){
        principal_move();
    }

    if(baldix < width && baldiy < height){
        baldi_move();
    }

    if(gx < width && gy < height){
        Gottasweep_move();
    }    

    if(bx < width && by < height){
        b_move();
    }

    if (ax < width && ay < height){
        art_move();    
    }

        

}



function Gottasweep_move(){
    image(Gottasweep, gx, gy, 350, 350);
    if(gx % 2 == 0){
        gx = gx + .9;
    }else{
        gx = gx - random(1, 6);
    }
}

function baldi_move(){
    image(Baldi, baldix, baldiy, 350, 350);
    baldix = baldix - 0.85;
    baldiy = baldiy + 0.5;
    

}

function principal_move(){
    image(img, px, py, 350, 350);
    if(px % 2 == 0){
        px = px + 10;
    }else{
        px = px - 10;
    }

    if(py % 3 == 0){
        py = py + 10;
    }else{
        py = py  + 10;
     }
}

function Playtime_move(){
    image(play, Px, Py, 350);
    if(Px%2==0){
        Px = Px + 10;
    }else{
        Px = Px - 10;
    }
    
    if(Py = Py%3==0){
        Py = Py * random(1, 6)*1.2;
    }else{
        Py = -Py * random(1, 6)*2;
    }
}
    
function First_Prize(){
    image(prize, firstx, firsty, 150, 300);

    if(firstx % 2 == 0){
        firstx = firstx + 4;
    }else{
        if(firstx > 400){
            firstx = firstx - 15;
        }else{
            firstx + 8;
        }
    }
    

    if(firsty % 3 == 0){
        firsty = firsty*1;
    }else{
        firsty = -firsty*2;
    }
}


function art_move(){

    if(firstx < width){
        image(art, ax, ay, 150, 300);
    }else{
        image(art2, angryx, angryy, 150, 300);
        ax=-150;

        angryx = angryx + random(-5, 8);
        angryy = angryy + random(-5, 8);

        if(isPlayed == false){
            art_sound();

        }
    }
}

function art_sound(){
    artSound.play();
    isPlayed = true;

}
function b_move(){
    image(Bully, bx, by, 150, 300);
    if(ax < 0){
        bx = -150;
    } 
}

function Filenametwo_move(){
    image(Filenametwo, Fx, Fy, 150, 300);
   Fx = Fx+1; 
   Fy = Fy+1; 
}

function cloud_move(){
    image(cloud, Cx, Cy, 200, 280);
    Cx = Cx+3;
    Cy = Cy+3;
}
