let angle = 0;
let cat;
let cheezit;
let trex;

function preload(){
	cat = loadImage("cat.jpeg");
	cheezit = loadImage("cheezits.jpeg");
	trex = loadImage("T.png");
}

function setup(){
	createCanvas(windowWidth, windowHeight, WEBGL);
}

function draw(){
	background(0);
	ambientLight(255);
	directionalLight(255, 255, 255, 0, 0, 1);
	//rotateX(angle);
	//rotateY(angle*1.3);
	//rotateZ(angle*0.7);
	texture(cat);
	translate(-50, -50);
	beginShape();
	vertex(0, 0, 0, 1, 0);
	vertex(100, 0, 0, 0, 0);
	vertex(mouseX, mouseY, 0, 0, 1);
	vertex(0, 100, 0, 0, 1);
	endShape(CLOSE);
	angle+=0.03;

	texture(cheezit);
	translate(-50, -50);
	beginShape();
	vertex(0, 0, 0, 0, 0);
	vertex(100, 0, 0, 1, 0);
	vertex(mouseX, mouseY, 0, 1, 1);
	vertex(0, 100, 0, 0, 1);
	endShape(CLOSE);
	angle+=0.03;

	texture(trex);
	translate(-50, -50);
	beginShape();
	vertex(0, 0, 0, 0, 0);
	vertex(100, 0, 0, 1, 0);
	vertex(mouseX, mouseY, 0, 1, 1);
	vertex(0, 100, 0, 0, 1);
	endShape(CLOSE);
	angle+=0.03;
}
