var r = 255;
var b = 255;
var img;
function preload(){
   img = loadImage('baldi.png'); 
}

function setup() {
    createCanvas(windowWidth, windowHeight);
}

function draw() {
    r = map(mouseX, 0, 600, 0, 255);
    b = map(mouseY, 0, 600, 255, 0);
    background(r, 255, b);
    map(100, 100, 50, 50, 50);
    image(img, mouseX, mouseY, 300, 300);
    
}
