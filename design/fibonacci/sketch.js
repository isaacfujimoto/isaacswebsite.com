var fibonacci =[1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233];
var scalar = 5;
var i;
var xpos;
var ypos;
var length;
var next_length
var number; 

const direction = {
    LEFT: 'left',
    DOWN: 'down',
    RIGHT: 'right',
    UP: 'up'
}

function setup(){
    createCanvas(windowWidth, windowHeight);
    i = 0;
    xpos = 0;
    ypos = 0;

    write();

    translate(width/2, height/2);
    length = fibonacci[0] * scalar;



    draw_box(fibonacci[0], null);
    draw_box(fibonacci[1], direction.LEFT);
    draw_box(fibonacci[2], direction.DOWN);
    draw_box(fibonacci[3], direction.RIGHT);
    draw_box(fibonacci[4], direction.UP);
    draw_box(fibonacci[5], direction.LEFT);
    draw_box(fibonacci[6], direction.DOWN);
    draw_box(fibonacci[7], direction.RIGHT);
    draw_box(fibonacci[8], direction.UP);
    draw_box(fibonacci[9], direction.LEFT);
    draw_box(fibonacci[10], direction.DOWN);
    draw_box(fibonacci[11], direction.RIGHT);
}



function draw(){

 }


function write(){
    for(i = 0; i < fibonacci.length; i++){
        text(fibonacci[i], 0*10, i * 10);
    }
}


function draw_box(number, dir){

    last_length = length;
    length = number * scalar;

    switch(dir){
        case direction.LEFT:
            xpos = xpos - length;
            ypos = ypos;
            break;
        case direction.DOWN:
            xpos = xpos;
            ypos = ypos + last_length;
            break;
        case direction.RIGHT:
            xpos = xpos + last_length;
            ypos = ypos + last_length - length;
            break;
        case direction.UP:
            xpos = xpos + last_length - number * scalar;
            ypos = ypos - number * scalar;
            break;
        default:
            xpos = xpos;
            ypos = ypos;
    }
        
    rect(xpos, ypos, length, length);
} 
